This shell script enables and disables the skyrim script extender on linux.

SKSE does not show the normal skyrim pre-game menu which prevents the data packs from being enabled and disabled. Normally, one would have to rename the SKSE launcher to SkyrimLauncher.exe and the original one to something else. This takes time and is a faff so this script automates that. 

To set it up, make sure that the normal SkyrimLauncher.exe is named as such and that the SKSE edition is called "SkyrimLauncher.exe.skse".
After this, the script should run and will write to stdout if it's successful or not.

This program is licenced under the MIT licence, see LICENCE for details.